INTRODUCTION
------------

Entityreference formatters provides several formatters for entityreference
fields. Without additional contributed modules it provides the
'vertical tabs' formatter.


RECOMMENDED MODULES
-------------------

 * Field group (https://www.drupal.org/project/field_group):
   When enabled, the following formatters become available:
   - horizontal tabs
   - accordion (planned)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * On the 'Manage display' page of the entity containing your entityreference
   field, select 'Tabs' as a formatter for your field.

 * Configure which view mode should be used to render the referenced entity.
   (defaults to 'default')

 * Configure if horizontal or vertical tabs should be rendered. (defaults to
   horizontal)
