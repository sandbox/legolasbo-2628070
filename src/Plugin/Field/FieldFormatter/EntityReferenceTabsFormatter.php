<?php

/**
 * @file
 * Contains \Drupal\er_formatters\Plugin\Field\FieldFormatter\EntityReferenceTabsFormatter.
 */

namespace Drupal\er_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormState;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\VerticalTabs;
use Drupal\field_group\Element\HorizontalTabs;

/**
 * Plugin implementation of the entity reference tabs formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_tabs",
 *   label = @Translation("Tabs"),
 *   description = @Translation("Display the referenced entities as horizontal or vertical tabs."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceTabsFormatter extends EntityReferenceEntityFormatter {

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $type = $this->getTypeSetting() . '_tabs';
    $element = [
      '#parents' => [$type],
      '#type' => $type,
      '#title' => '',
      '#theme_wrappers' => [$type],
      '#default_tab' => '',
    ];

    foreach ($this->getEntitiesToView($items, $langcode) as $entity) {
      $tab = [
        '#type' => 'details',
        '#title' => $entity->label(),
        '#description' => '',
      ];

      $view_builder = \Drupal::entityTypeManager()
        ->getViewBuilder($entity->getEntityTypeId());
      $tab[] = $view_builder->view($entity, $this->getSetting('view_mode'), $langcode);
      $element['#children'][] = $tab;
    }

    // Tabs are expecting to be rendered in a form context. Since we're re-using
    // their formatters for our non-form purpose, we have to pass a fake form
    // state to match the expected arguments.
    $form_state = new FormState();
    if ($this->getTypeSetting() == 'vertical') {
      $complete_form = [];
      $element = VerticalTabs::processVerticalTabs($element, $form_state, $complete_form);
      // Make sure the group has 1 child. This is needed to succeed at
      // VerticalTabs::preRenderVerticalTabs(). Skipping this would force us to
      // move all child groups to this array, making it an un-nestable.
      $element['group']['#groups'][$type] = [0 => []];
      $element['group']['#groups'][$type]['#group_exists'] = TRUE;
    }
    else {
      $element = HorizontalTabs::processHorizontalTabs($element, $form_state, FALSE);
    }

    // Prevent fallback to field render array by hiding any children.
    foreach (Element::children($element) as $child) {
      $element['#hidden_children'][$child] = $element[$child];
      unset($element[$child]);
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    if (\Drupal::moduleHandler()->moduleExists('field_group')) {
      $defaults = ['type' => 'horizontal'];
    }
    else {
      $defaults = ['type' => 'vertical'];
    }

    return $defaults + parent::defaultSettings();
  }


  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['type'] = array(
      '#type' => 'select',
      '#options' => $this->getTabTypes(),
      '#title' => t('Type'),
      '#default_value' => $this->getTypeSetting(),
      '#required' => TRUE,
    );

    return $elements;
  }

  /**
   * Retrieves the type of formatter to use.
   *
   * @return string
   *   The type of tab to use, either horizontal or vertical.
   */
  protected function getTypeSetting() {
    return !\Drupal::moduleHandler()
      ->moduleExists('field_group') ? 'vertical' : (string) $this->getSetting('type');
  }

  /**
   * Retrieve which tab types can be configured.
   *
   * @return array
   *   The possible tab types to configure.
   */
  protected function getTabTypes() {
    $types = [];

    if (\Drupal::moduleHandler()->moduleExists('field_group')) {
      $types['horizontal'] = t('Horizontal tabs');
    }

    $types['vertical'] = t('Vertical tabs');

    return $types;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $types = $this->getTabTypes();
    $summary[] = t('Rendered as @mode', array('@mode' => $types[$this->getTypeSetting()]));

    return $summary;
  }

}
